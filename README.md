This is a template for X51 GitPitch briefings.
You can say an example briefing using this template at: https://gitlab.com/x51squadron/test-briefing

The gitpitch code is very simple, and looks like this:

```markdown

# Test brief for X51

![Logo](assets/img/x51_logo_tiny.png)

An easy way to share briefings before events

#HSLIDE

### MISSION
- Do ""pre-flight""
- Do a thing
- Destroy some thing
- Don't die
- Land safely

#HSLIDE

### Include briefing pics
![Logo](assets/img/brief1.png)

#HSLIDE

### Organise flights
| Blue Flight (F-15C) | Red Flight (Av-8B)|
|---------------------|-------------------|
| Dragon              | FireBlaze         |
|  Divine             | Sang              |
| ButterCup           | GrendalKitty      |

#HSLIDE

### Include training material
![Video](https://youtube.com/embed/5gu49fpHRIw)
```
Compiling to this:
https://gitpitch.com/x51squadron/test-briefing?grs=gitlab

